// See LICENSE.MD for license information.

'use strict';

/********************************
 Dependencies
 ********************************/
var express = require('express'),// server middleware
    multer  = require('multer'),
    mongoose = require('mongoose'),// MongoDB connection library
    bodyParser = require('body-parser'),// parse HTTP requests
    passport = require('passport'),// Authentication framework
    LocalStrategy = require('passport-local').Strategy,
    expressValidator = require('express-validator'), // validation tool for processing user input
    cookieParser = require('cookie-parser'),
    session = require('express-session'),
    MongoStore = require('connect-mongo/es5')(session), // store sessions in MongoDB for persistence
    bcrypt = require('bcrypt'), // middleware to encrypt/decrypt passwords
    sessionDB,
    fs = require('fs-extra'),
    cfenv = require('cfenv'),// Cloud Foundry Environment Variables
    appEnv = cfenv.getAppEnv(),// Grab environment variables

    User = require('./server/models/user.model'),
    Course = require('./server/models/courses.model'),
    Assignment = require('./server/models/assignments.model'),
    Submission = require('./server/models/submission.model');

/********************************
 Local Environment Variables
 ********************************/
if(appEnv.isLocal){
    require('dotenv').load();// Loads .env file into environment
}

/********************************
 MongoDB Connection
 ********************************/

//Detects environment and connects to appropriate DB
if(appEnv.isLocal){
    mongoose.connect(process.env.LOCAL_MONGODB_URL);
    sessionDB = process.env.LOCAL_MONGODB_URL;
    console.log('Your MongoDB is running at ' + process.env.LOCAL_MONGODB_URL);
}
else{
    console.log('Unable to connect to MongoDB.');
}

/********************************
 Express Settings
 ********************************/
var app = express();//, server = http.createServer(app);
app.enable('trust proxy');
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(expressValidator()); // must go directly after bodyParser
app.use(cookieParser());
app.use(session({
    secret: process.env.SESSION_SECRET || 'this_is_a_default_session_secret_in_case_one_is_not_defined',
    resave: true,
    store: new MongoStore({
        url: sessionDB,
        autoReconnect: true
    }),
    saveUninitialized : false,
    cookie: { secure: true }
}));
app.use(passport.initialize());
app.use(passport.session());


/********************************
 Passport Middleware Configuration
 ********************************/
passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});

passport.use(new LocalStrategy(function(username, password, done) {
    User.findOne({ username: username }, function (err, user) {
        if (err) {
            return done(err);
        }
        if (!user) {
            return done(null, false, { message: 'Incorrect username.' });
        }
        // validatePassword method defined in user.model.js
        if (!user.validatePassword(password, user.password)) {
            return done(null, false, { message: 'Incorrect password.' });
        }
        return done(null, user);
    });
}));

/********************************
 Multer Middleware
 ********************************/
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        let type = req.params.type;
        let path = `./uploads/${type}`;
        fs.mkdirsSync(path);
        cb(null, path);
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});
const upload = multer({
    storage: storage
}).any();

/********************************
 Routing
 ********************************/

// Entry point for Angular
app.get('/', function (req, res){
    res.sendfile('index.html');
});

//Multer upload point
app.post('/upload/:type', function (req, res) {
    upload(req, res, function(err){

    });
});

/********************************
 Account Routing
 ********************************/
// Account login
app.post('/account/login', function(req,res){

    // Validation prior to checking DB. Front end validation exists, but this functions as a fail-safe
    req.checkBody('username', 'Username is required').notEmpty();
    req.checkBody('password', 'Password is required').notEmpty();
    var errors = req.validationErrors(); // returns an object with results of validation check
    if (errors) {
        res.status(401).send('Username or password was left empty. Please complete both fields and re-submit.');
        return;
    }

    // Create session if username exists and password is correct
    passport.authenticate('local', function(err, user) {
        if (err) { return next(err); }
        if (!user) { return res.status(401).send('User not found. Please check your entry and try again.'); }
        req.logIn(user, function(err) { // creates session
            if (err) { return res.status(500).send('Error saving session.'); }
            var userInfo = {
                username: user.username,
                name : user.name,
                email : user.email,
                isInstructor : user.isInstructor
            };
            return res.json(userInfo);
        });
    })(req, res);

});

// Account creation
app.post('/account/create', function(req,res){

    // 1. Input validation. Front end validation exists, but this functions as a fail-safe
    req.checkBody('username', 'Username is required').notEmpty();
    req.checkBody('password', 'Password is required').notEmpty();
    req.checkBody('name', 'Name is required').notEmpty();
    req.checkBody('email', 'Email is required and must be in a valid form').notEmpty().isEmail();
    var errors = req.validationErrors(); // returns an array with results of validation check
    if (errors) {
        res.status(400).send(errors);
        return;
    }

    // 2. Hash user's password for safe-keeping in DB
    var salt = bcrypt.genSaltSync(10),
        hash = bcrypt.hashSync(req.body.password, salt);

    // 3. Create new object that store's new user data
    var user = new User({
        username: req.body.username,
        password: hash,
        email: req.body.email,
        name: req.body.name
    });

    // 4. Store the data in MongoDB
    User.findOne({ username: req.body.username }, function(err, existingUser) {
        if (existingUser) {
            return res.status(400).send('That username already exists. Please try a different username.');
        }
        user.save(function(err) {
            if (err) {
                console.log(err);
                res.status(500).send('Error saving new account (database error). Please try again.');
                return;
            }
            res.status(200).send('Account created! Please login with your new account.');
        });
    });

});

//Account deletion
app.post('/account/delete', authorizeRequest, function(req, res){

    User.remove({ username: req.body.username }, function(err) {
        if (err) {
            console.log(err);
            res.status(500).send('Error deleting account.');
            return;
        }
        req.session.destroy(function(err) {
            if(err){
                res.status(500).send('Error deleting account.');
                console.log("Error deleting session: " + err);
                return;
            }
            res.status(200).send('Account successfully deleted.');
        });
    });
});

//Account deletion
app.post('/account/promote', authorizeRequest, function(req, res){

    User.findOne({ username: req.body.username }, function(err, user) {
        if(err){
            res.status(500).send('Error promoting (database error). Please try again.');
            return;
        }
        user.isInstructor = true;
        user.save(function(err){
            if (err) {
                console.log(err);
                res.status(500).send('Error promoting (database error). Please try again.');
                return;
            }
        });
        res.status(200).send('Account successfully promoted.');
    });
});


// Account update
app.post('/account/update', authorizeRequest, function(req,res){

    // 1. Input validation. Front end validation exists, but this functions as a fail-safe
    req.checkBody('username', 'Username is required').notEmpty();
    req.checkBody('password', 'Password is required').notEmpty();
    req.checkBody('name', 'Name is required').notEmpty();
    req.checkBody('email', 'Email is required and must be in a valid form').notEmpty().isEmail();

    var errors = req.validationErrors(); // returns an object with results of validation check
    if (errors) {
        res.status(400).send(errors);
        return;
    }

    // 2. Hash user's password for safe-keeping in DB
    var salt = bcrypt.genSaltSync(10),
        hash = bcrypt.hashSync(req.body.password, salt);

    // 3. Store updated data in MongoDB
    User.findOne({ username: req.body.username }, function(err, user) {
        if (err) {
            console.log(err);
            return res.status(400).send('Error updating account.');
        }
        user.username = req.body.username;
        user.password = hash;
        user.email = req.body.email;
        user.name = req.body.name;
        user.save(function(err) {
            if (err) {
                console.log(err);
                res.status(500).send('Error updating account.');
                return;
            }
            res.status(200).send('Account updated.');
        });
    });

});

// Account logout
app.post('/account/logout', authorizeRequest, function (req,res){

    // Destroys user's session
    if (!req.body.username)
        res.status(400).send('User not logged in.');
    else {
        req.session.destroy(function(err) {
            if(err){
                res.status(500).send('Sorry. Server error in logout process.');
                console.log("Error destroying session: " + err);
                return;
            }
            res.status(200).send('Success logging user out!');
        });
    }
});

/********************************
 Course Routing
 ********************************/

//Course Creation
app.post('/courses/create', function(req, res){
    // 1. Input validation. Front end validation exists, but this functions as a fail-safe
    req.checkBody('name', 'Course Name is required').notEmpty();
    req.checkBody('description', 'Description is required').notEmpty();
    req.checkBody('CRN', 'CRN is required').notEmpty();
    req.checkBody('syllabus', 'A syllabus is required').notEmpty();
    req.checkBody('instructor', 'Invalid session detected').notEmpty();
    var errors = req.validationErrors(); // returns an array with results of validation check
    if (errors) {
        res.status(400).send(errors);
        return;
    }

    // 2. Check user credentials
    User.findOne({username: req.body.instructor}, function(err, user){
        //TODO re-enable after testing
        // if(user.isInstructor === false){
        //     res.status(403).send('Error, you are not authorised to create a new Course');
        //     return;
        // }

        // 3. Create new object that store's new user data
        var course = new Course({
            name: req.body.name,
            description: req.body.description,
            CRN: req.body.CRN,
            syllabus: req.body.syllabus,
            instructor: user._id
        });


        // 4. Store the data in MongoDB
        Course.findOne({ CRN: req.body.CRN }, function(err, existingCourse) {
            if (existingCourse) {
                return res.status(400).send('That CRN already exists. Please try a different CRN.');
            }

            user.classes.push(course);
            user.save(function(err) {
                if (err) {
                    console.log(err);
                    res.status(500).send('Error registering new course (database error). Please try again.');
                    return;
                }
            });

            course.save(function(err) {
                if (err) {
                    console.log(err);
                    res.status(500).send('Error saving new course (database error). Please try again.');
                    return;
                }
                res.status(200).send('Course created! Please add an assignment to your course.');
            });
        });
    });
});

//Register users to courses
app.post('/courses/register', authorizeRequest, function (req, res){
    User.findOne({username: req.body.username}, function (err, user) {
        if(!user){
            return res.status(400).send('Invalid user id. Please log back in and try again.');
        }
        Course.findOne({CRN: req.body.CRN}, function (err, course){

            //Check if we are registered already
            // var isInArray = user.classes.some(function (aclass) {
            //     return aclass.equals(course._id);
            // });

            // if(isInArray){
            //     res.status(400).send('You\'re already registered for this class!');
            //     return;
            // }

            user.classes.push(course);
            user.save(function(err){
                if (err) {
                    console.log(err);
                    res.status(500).send('Error registering new course (database error). Please try again.');
                    return;
                }
                res.status(200).send('Success registering user course!');
            });
        });
    });
});

//List courses
app.get('/courses', function (req, res) {
    if(!req.query.username){
        res.status(400).send('Error loading courses for the requested user, please try again.');
    }

    User.findOne({ username: req.query.username}, function(err, user){
        if(!user){
            return res.status(400).send('No user associated with that id.');
        }
        if(err){
            console.log(err);
            res.status(500).send('Error retrieving user courses. Please try again.');
            return;
        }
        User.findOne({username: req.query.username}).populate({path: 'classes', populate: {path: 'instructor assignments'}}).exec(function (err, user){
            res.status(200).send(user.classes);
        });
    });


});

// GET single course
app.get('/courses/:CRN', function (req, res){
    if(!req.query.username){
        res.status(400).send('Error loading requested course, please try again.');
    }

    Course.findOne({CRN : req.params.CRN}).populate({path: 'assignments instructor'}).exec(function(err, course){
        if(err){
            console.log(err);
            res.status(500).send('Unable to retrieve course (database error). Please retry.');
            return;
        }
        res.status(200).send(course);
    });
});

//Course Deletion
app.post('/courses/delete', function(req, res){
    User.findOne({username: req.body.username}, function(err, user){
        if(user.isInstructor){
            Course.remove({CRN: req.body.CRN}, function(err){
                if (err) {
                    console.log(err);
                    res.status(500).send('Error deleting course.');
                    return;
                }
                res.status(200).send('Course successfully deleted.');
            });
        }
        else{
            res.status(403).send('You are not authorized to delete a course');
        }
    });
});

app.post('/courses/update', authorizeRequest, function (req, res){

    // 1. Input validation. Front end validation exists, but this functions as a fail-safe
    req.checkBody('syllabus', 'Syllabus is required').notEmpty();
    req.checkBody('description', 'Description is required').notEmpty();
    req.checkBody('name', 'Name is required').notEmpty();
    req.checkBody('newCRN', 'A CRN is required and must be in a valid form').notEmpty();

    var errors = req.validationErrors(); // returns an object with results of validation check
    if (errors) {
        res.status(400).send(errors);
        return;
    }

    // 3. Store updated data in MongoDB
    User.findOne({ username: req.body.username }, function(err, user) {
        if (err) {
            console.log(err);
            return res.status(400).send('Error updating course.');
        }
        if (!user.isInstructor){
            res.status(403).send('You are not authorised to modify a course');
            return;
        }
        Course.findOne({CRN: req.body.CRN}, function(err, course){
            course.description = req.body.description;
            course.CRN = req.body.newCRN;
            course.syllabus = req.body.syllabus;
            course.name = req.body.name;

            course.save(function(err) {
                if (err) {
                    console.log(err);
                    res.status(500).send('Error updating course.');
                    return;
                }
                res.status(200).send('Course updated.');
            });
        });

    });
});

//TODO Individual UPDATE

/********************************
 Assignment Routing
 ********************************/
//Create new assignment
app.post('/courses/:CRN/assignments/create', authorizeRequest, function (req, res){

    // 1. Input validation. Front end validation exists, but this functions as a fail-safe
    req.checkBody('rubric', 'A rubric is required').notEmpty();
    req.checkBody('description', 'Description is required').notEmpty();
    req.checkBody('max_score', 'A maximum score is required').notEmpty();
    req.checkBody('filename', 'A file for this assignment is required').notEmpty();

    var errors = req.validationErrors(); // returns an array with results of validation check
    if (errors) {
        res.status(400).send(errors);
        return;
    }

    // 2. Check user credentials
    User.findOne({username: req.body.username}, function(err, user){

        //TODO re-enable after testing
        // if(user.isInstructor === false){
        //     res.status(403).send('Error, you are not authorised to create a new Course');
        //     return;
        // }

        Course.findOne({ CRN: req.params.CRN }, function(err, course) {
            if(!course){
                res.status(400).send('Unable to retrieve course info.');
                return;
            }

            // 3. Create new object that store's new assignment data
            var assignment = new Assignment({
                rubric: req.body.rubric,
                description: req.body.description,
                filename: req.body.filename,
                max_score: req.body.max_score,
                CRN: course._id,
            });

            // 4. Update course to reflect new assignment
            course.assignments.push(assignment);
            course.save(function (err) {
                if (err) {
                    console.log(err);
                    res.status(500).send('Error saving new assignment (database error). Please try again.');
                    return;
                }
            });

            // 5. Store the data in MongoDB
            assignment.save(function(err) {
                if (err) {
                    console.log(err);
                    res.status(500).send('Error saving new assignment (database error). Please try again.');
                    return;
                }
                res.status(200).send('Assignment created!');
            });
        });
    });
});

//Get all assignments for a class
app.get('/courses/:CRN/assignments', authorizeRequest, function (req, res){
    if(!req.query.username){
        res.status(400).send('Error loading courses for the requested user, please try again.');
    }

    Course.findOne({CRN : req.params.CRN}).populate({path: 'assignments', populate: {path: 'CRN submissions'}}).exec(function(err, course){
        if(err){
            console.log(err);
            res.status(500).send('Unable to retrieve assignments (database error). Please retry.');
            return;
        }
        res.status(200).send(course.assignments);
    });
});

//Get one assignment
app.get('/courses/:CRN/assignments/:id', authorizeRequest, function (req, res) {
    if(!req.query.username){
        res.status(400).send('Error loading requested course, please try again.');
    }
    User.findOne({username: req.query.username}, function(err, user){
        if(err){
            console.log(err);
            res.status(500).send('Unable to retrieve course (database error). Please retry.');
            return;
        }
        Assignment.findOne({_id : req.params.id}).populate({path: 'submissions', match: {username: user._id}}).exec(function(err, course){
            if(err){
                console.log(err);
                res.status(500).send('Unable to retrieve course (database error). Please retry.');
                return;
            }
            res.status(200).send(course);
        });
    });

});

//Delete one assignment
app.post('/courses/:CRN/assignments/delete', authorizeRequest, function (req, res) {
    User.findOne({username: req.body.username}, function(err, user) {
        if (user.isInstructor) {
            Assignment.remove({_id: req.body.id}, function (err) {
                if (err) {
                    console.log(err);
                    res.status(500).send('Error deleting course.');
                    return;
                }
                res.status(200).send('Assignment successfully deleted.');
            });
        } else {
            res.status(403).send('You are not authorized to delete an assignment');
        }
    });
});

//TODO Update

/********************************
 Submission Routing
 ********************************/
//Create one submission
app.post('/courses/:CRN/assignments/:id/submissions/create', authorizeRequest, function (req, res){

    // 1. Input validation. Front end validation exists, but this functions as a fail-safe
    req.checkBody('filename', 'A file for this submission is required').notEmpty();

    var errors = req.validationErrors(); // returns an array with results of validation check
    if (errors) {
        res.status(400).send(errors);
        return;
    }

    // 2. Check user credentials
    User.findOne({username: req.body.username}, function(err, user){

        Assignment.findOne({ _id: req.params.id }, function(err, assignment) {
            if(!assignment){
                res.status(400).send('Unable to retrieve course info.');
                return;
            }


            // 3. Create new object that store's new assignment data
            var submission = new Submission({
                description: req.body.description,
                filename: req.body.filename,
                assignmentID: req.params.id,
                username: user._id
            });

            if(req.body.submissionText){
                submission.submissionText = req.body.submissionText;
            }

            // 4. Update course to reflect new assignment
            assignment.submissions.push(submission);
            assignment.save(function (err) {
                if (err) {
                    console.log(err);
                    res.status(500).send('Error saving new submission (database error). Please try again.');
                    return;
                }
            });

            // 5. Store the data in MongoDB
            submission.save(function(err) {
                if (err) {
                    console.log(err);
                    res.status(500).send('Error saving new submission (database error). Please try again.');
                    return;
                }
                res.status(200).send('Submission made at ' + Date.now());
            });
        });
    });
});

//Submit a comment
app.post('/courses/:CRN/assignments/:id/submissions/:subid/newComment', authorizeRequest, function (req, res) {

    req.checkBody('body', 'A body for this comment is required').notEmpty();

    var errors = req.validationErrors(); // returns an array with results of validation check
    if (errors) {
        res.status(400).send(errors);
        return;
    }

    User.findOne({username: req.body.username}, function(err, user){

        Submission.findOne({ _id: req.params.subid }, function(err, submission) {
            if(!submission){
                res.status(400).send('Unable to retrieve submission info.');
                return;
            }

            var newComment = {body: req.body.body, date: Date.now(), poster: user._id};

            // 4. Update submission to reflect new comment
            submission.comments.push(newComment);

            // 5. Store the data in MongoDB
            submission.save(function(err) {
                if (err) {
                    console.log(err);
                    res.status(500).send('Error saving new comment (database error). Please try again.');
                    return;
                }
                res.status(200).send('Submission comment made at ' + Date.now());
            });
        });
    });
});

app.get('/courses/:CRN/assignments/:id/submissions', authorizeRequest, function (req, res){
    if(!req.query.username){
        res.status(400).send('Error loading submissions for the requested user, please try again.');
    }
    User.findOne({username: req.query.username}, function (err, user) {
        //Instructors see all assignments
        if(user.isInstructor){
            Submission.find({assignmentID : req.params.id}).populate({path: 'assignmentID username', populate: {path: 'CRN'}}).exec(function(err, submission){
                if(err){
                    console.log(err);
                    res.status(500).send('Unable to retrieve submissions (database error). Please retry.');
                    return;
                }
                res.status(200).send(submission);
            });
        }
        else{
            res.status(403).send('You are not authorized to view all submissions');
        }
    });
});

//Get one submissions for a class
app.get('/courses/:CRN/assignments/:id/submissions/:subid', authorizeRequest, function (req, res){
    if(!req.query.username){
        res.status(400).send('Error loading submissions for the requested user, please try again.');
    }
    Submission.findOne({_id : req.params.subid}).populate({path: 'assignmentID username', populate: {path: 'CRN'}}).exec(function(err, submission){
        if(err){
            console.log(err);
            res.status(500).send('Unable to retrieve submissions (database error). Please retry.');
            return;
        }
        res.status(200).send(submission);
    });

});

//Delete one submission
app.post('/courses/:CRN/assignments/:id/submissions/:subid/delete', authorizeRequest, function (req, res) {
    User.findOne({username: req.body.username}, function(err, user) {
        Submission.remove({_id: req.params.subid}, function (err) {
            if (err) {
                console.log(err);
                res.status(500).send('Error deleting course.');
                return;
            }
            res.status(200).send('Submission successfully deleted.');
        });
    });
});


// Custom middleware to check if user is logged-in
function authorizeRequest(req, res, next) {
    if (req.body.username) {
        next();
    }
    else if (req.query.username){
        next();
    }
    else {
        res.status(401).send('Unauthorized. Please login.');
    }
}

// Protected route requiring authorization to access.
app.get('/protected', authorizeRequest, function(req, res){
    res.send("This is a protected route only visible to authenticated users.");
});

/********************************
 Ports
 ********************************/
app.listen(9003, function() {
    console.log("Node server running on http://localhost:9003");
});