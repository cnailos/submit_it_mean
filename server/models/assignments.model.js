'use strict';

/********************************
 Dependencies
 ********************************/
var mongoose = require('mongoose');

/********************************
 Create User Account Schema
 ********************************/
var assignmentSchema = new mongoose.Schema({
    rubric: {type: String, required: false},
    description: {type: String, required: true},
    filename: {type: String, required: true},
    max_score: {type: Number, required: true},
    CRN: {type: mongoose.Schema.Types.ObjectId, ref: 'Course'},
    submissions: [{type: mongoose.Schema.Types.ObjectId, ref: 'Submission'}]
},
    {usePushEach : true});

assignmentSchema.pre('remove', function(next) {
    var assignment = this;
    assignment.model('Submission').deleteMany({ assignmentID: assignment._id }, next);
});

module.exports = mongoose.model('Assignment', assignmentSchema);