'use strict';

/********************************
 Dependencies
 ********************************/
var mongoose = require('mongoose');

/********************************
 Create Courses Account Schema
 ********************************/
var courseSchema = new mongoose.Schema({
    CRN: {type: Number, required: true, unique: true},
    instructor: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}, //Basically a FK to user
    syllabus: {type: String, required: false},
    description: {type: String, required: true},
    name: {type: String, required: true},
    assignments: [{type: mongoose.Schema.Types.ObjectId, ref: 'Assignment'}] //Parent holds child data for ease of access
},
    {usePushEach : true});


module.exports = mongoose.model('Course', courseSchema);