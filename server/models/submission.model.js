'use strict';

/********************************
 Dependencies
 ********************************/
var mongoose = require('mongoose')

/********************************
 Create User Account Schema
 ********************************/
var submissionSchema = new mongoose.Schema({
    filename: {type: String, required: true},
    submissionTime: {type: Date, default: Date.now},
    submissionText: {type: String},
    earnedScore: {type: Number},
    //Mongo is powerful here because we can essentially create new document types on the fly. This is a collection that holds metadata for a "comment"
    comments: [{body: String, date: Date, poster: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}}],
    //Basically a FK to user
    username: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    //Basically a FK to assignments
    assignmentID: {type: mongoose.Schema.Types.ObjectId, ref: 'Assignment'}
},
    {usePushEach : true});


module.exports = mongoose.model('Submission', submissionSchema);