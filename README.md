Development Notes

1. All GET requests must include the username as a query string parameter. For example a get to /courses must be formatted as /courses?username={username}

2. All POST requests must include the username in the request body.

3. Any authenticated User can modify content that is not their own

4. Solution to the above would be client server authentication with something like a JWT

5. Ref ObjectIds are persisted in parents even after removal of the child, however when calling populate() these values are not retrieved.