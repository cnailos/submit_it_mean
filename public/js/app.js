// See LICENSE.MD for license information.

var app = angular.module('MEANapp', ['ngRoute', 'ngStorage']);

/*********************************
 Controllers
 *********************************/

app.controller('HeaderController', function($scope, $localStorage, $sessionStorage, $location, $http){

    // Set local scope to persisted user data
    $scope.user = $localStorage;

    // Logout function
    $scope.logout = function(){
        $http({
            method: 'POST',
            url: '/account/logout',
            data: {
                'username' : $scope.user.user.username
            }
        })
            .success(function(response){
                alert(response);
                $localStorage.$reset();
                $location.path('/');
            })
            .error(function(response){
                    alert(response);
                    $location.path('/account/login');
                }
            );
    };
});

app.controller('HomeController', function($scope, $localStorage, $sessionStorage){
});

app.controller('LoginController', function($scope, $localStorage, $sessionStorage, $location, $http){

    // Login submission
    $scope.submitLogin = function(){

        // Login request
        $http({
            method: 'POST',
            url: '/account/login',
            data: {
                'username': $scope.loginForm.username,
                'password': $scope.loginForm.password
            }
        })
            .success(function(response){
                // $localStorage persists data in browser's local storage (prevents data loss on page refresh)
                $localStorage.status = true;
                $localStorage.user = response;
                $location.path('/');
            })
            .error(function(){
                    alert('Login failed. Check username/password and try again.');
                }
            );
    };

    // Redirect to account creation page
    $scope.createAccount = function(){
        $location.path('/account/create');
    }
});

app.controller('CreateAccountController', function($scope, $localStorage, $sessionStorage, $http, $location){

    // Create account
    $scope.submitForm = function(){
        $http({
            method: 'POST',
            url: '/account/create',
            data: {
                'username': $scope.newUser.username,
                'password': $scope.newUser.password,
                'name' : $scope.newUser.name,
                'email' : $scope.newUser.email
            }
        })
            .success(function(response){
                alert(response);
                $location.path('/account/login');
            })
            .error(function(response){
                    // When a string is returned
                    if(typeof response === 'string'){
                        alert(response);
                    }
                    // When array is returned
                    else if (Array.isArray(response)){
                        // More than one message returned in the array
                        if(response.length > 1){
                            var messages = [],
                                allMessages;
                            for (var i = response.length - 1; i >= 0; i--) {
                                messages.push(response[i]['msg']);
                                if(response.length == 0){
                                    allMessages = messages.join(", ");
                                    alert(allMessages);
                                    console.error(response);
                                }
                            }
                        }
                        // Single message returned in the array
                        else{
                            alert(response[0]['msg']);
                            console.error(response);
                        }
                    }
                    // When something else is returned
                    else{
                        console.error(response);
                        alert("See console for error.");
                    }
                }
            );

    };
});

app.controller('AccountController', function($scope, $localStorage, $sessionStorage, $http, $location){

    // Create static copy of user data for form usage (otherwise any temporary changes will bind permanently to $localStorage)
    $scope.formData = $.extend(true,{},$localStorage.user);

    // Update user's account with new data
    $scope.updateAccount = function(){
        $http({
            method: 'POST',
            url: '/account/update',
            data: {
                'username': $scope.formData.username,
                'password': $scope.password,
                'name' : $scope.formData.name,
                'email' : $scope.formData.email
            }
        })
            .success(function(response){
                $localStorage.user = $scope.formData;
                alert(response);
            })
            .error(function(response){
                    // When a string is returned
                    if(typeof response === 'string'){
                        alert(response);
                    }
                    // When an array is returned
                    else if (Array.isArray(response)){
                        // More than one message returned in the array
                        if(response.length > 1){
                            var messages = [],
                                allMessages;
                            for (var i = response.length - 1; i >= 0; i--) {
                                messages.push(response[i]['msg']);
                                if(response.length == 0){
                                    allMessages = messages.join(", ");
                                    alert(allMessages);
                                    console.error(response);
                                }
                            }
                        }
                        // Single message returned in the array
                        else{
                            alert(response[0]['msg']);
                            console.error(response);
                        }
                    }
                    // When something else is returned
                    else{
                        console.error(response);
                        alert("See console for error.");
                    }
                }
            );
    };

    // Delete user's account
    $scope.deleteAccount = function(){
        var response = confirm("Are you sure you want to delete your account? This cannot be undone!");
        if(response == true){
            $http({
                method: 'POST',
                url: '/account/delete',
                data: {
                    'username': $scope.formData.username
                }
            })
                .success(function(response){
                    $localStorage.$reset();
                    alert(response);
                    $location.path('/');
                })
                .error(function(response){
                        alert(response);
                    }
                );
        }
    };
});

app.controller('CoursesController', function($scope, $localStorage, $sessionStorage, $http, $location){
    $http({
        method: 'GET',
        url: '/courses',
        params: {
            'username': $localStorage.user.username
        }
    })
        .success(function(response){
            $scope.isInstructor = $localStorage.user.isInstructor;
            $scope.courses = response;

            for(let res in $scope.courses){
                $scope.courses[res].link = '#/courses/' + response[res].CRN;
            }

            console.log( $scope.courses);
        })
        .error(function(response){
            alert(response);
        });
});

app.controller('SingleCourseController', function($scope, $localStorage, $sessionStorage, $http, $location, $routeParams){
    $http({
        method: 'GET',
        url: '/courses/' + $routeParams.CRN,
        params: {
            'username': $localStorage.user.username
        }
    })
        .success(function(response){
            $scope.assignments_url = '#/courses/' + $routeParams.CRN + '/assignments';
            $scope.course = response;
            console.log( $scope.course);
        })
        .error(function(response){
            alert(response);
        });
});

app.controller('CreateCourseController', function($scope, $localStorage, $sessionStorage, $http, $location){
    $scope.submitForm = function(){
        $http({
            method: 'POST',
            url: '/courses/create',
            data: {
                'description': $scope.newCourse.description,
                'CRN': $scope.newCourse.CRN,
                'name' : $scope.newCourse.name,
                'syllabus' : $scope.newCourse.syllabus,
                'instructor': $localStorage.user.username
            },
        }).success(function(response){
            alert(response);
            $location.path('/courses');
        })
            .error(function(response){
                    // When a string is returned
                    if(typeof response === 'string'){
                        alert(response);
                    }
                    // When array is returned
                    else if (Array.isArray(response)){
                        // More than one message returned in the array
                        if(response.length > 1){
                            var messages = [],
                                allMessages;
                            for (var i = response.length - 1; i >= 0; i--) {
                                messages.push(response[i]['msg']);
                                if(response.length == 0){
                                    allMessages = messages.join(", ");
                                    alert(allMessages);
                                    console.error(response);
                                }
                            }
                        }
                        // Single message returned in the array
                        else{
                            alert(response[0]['msg']);
                            console.error(response);
                        }
                    }
                    // When something else is returned
                    else{
                        console.error(response);
                        alert("See console for error.");
                    }
                }
            );
    };

});

app.controller('CreateAssignmentController', function($scope, $localStorage, $sessionStorage, $http, $location,$routeParams){

    $scope.submitForm = function(){
        $http({
            method: 'POST',
            url: '/courses/' + $routeParams.CRN + '/assignments/create',
            data: {
                'username' : $localStorage.user.username,
                'rubric' : $scope.newAssignment.rubric,
                'description' : $scope.newAssignment.description,
                'filename' : $scope.newAssignment.filename,
                'max_score' : $scope.newAssignment.max_score,
            },
        }).success(function(response){
            alert(response);
            $location.path('/courses/' + $routeParams.CRN + '/assignments');
        })
            .error(function(response){
                    // When a string is returned
                    if(typeof response === 'string'){
                        alert(response);
                    }
                    // When array is returned
                    else if (Array.isArray(response)){
                        // More than one message returned in the array
                        if(response.length > 1){
                            var messages = [],
                                allMessages;
                            for (var i = response.length - 1; i >= 0; i--) {
                                messages.push(response[i]['msg']);
                                if(response.length == 0){
                                    allMessages = messages.join(", ");
                                    alert(allMessages);
                                    console.error(response);
                                }
                            }
                        }
                        // Single message returned in the array
                        else{
                            alert(response[0]['msg']);
                            console.error(response);
                        }
                    }
                    // When something else is returned
                    else{
                        console.error(response);
                        alert("See console for error.");
                    }
                }
            );
    };

});

app.controller('AssignmentsController', function($scope, $localStorage, $sessionStorage, $http, $location, $routeParams){
    $http({
        method: 'GET',
        url: '/courses/' + $routeParams.CRN + '/assignments',
        params: {
            'username': $localStorage.user.username
        }
    })
        .success(function(response){
            $scope.isInstructor = $localStorage.user.isInstructor;
            $scope.assignments = response;
            $scope.create = '#/courses/' + $routeParams.CRN + '/assignments/create';
            for(let res in $scope.assignments){
                $scope.assignments[res].link = '#/courses/' + $routeParams.CRN + /assignments/ + response[res]._id;
                $scope.assignments[res].sublink = '#/courses/' + $routeParams.CRN + /assignments/ + response[res]._id + '/submissions';
            }

            console.log( $scope.assignments);
        })
        .error(function(response){
            alert(response);
        });
});

app.controller('SingleAssignmentController', function($scope, $localStorage, $sessionStorage, $http, $location, $routeParams){
    $scope.link = '#/courses/' + $routeParams.CRN + '/assignments/' + $routeParams.id + '/submissions/create';
    $http({
        method: 'GET',
        url: '/courses/' + $routeParams.CRN + '/assignments/' + $routeParams.id,
        params: {
            'username': $localStorage.user.username
        }
    })
        .success(function(response){
            $scope.assignment = response;
            console.log( $scope.assignment);
        })
        .error(function(response){
            alert(response);
        });
});

app.controller('SubmissionsController', function($scope, $localStorage, $sessionStorage, $http, $location, $routeParams){
    $http({
        method: 'GET',
        url: '/courses/' + $routeParams.CRN + '/assignments/' + $routeParams.id + '/submissions',
        params: {
            'username': $localStorage.user.username
        }
    })
        .success(function(response){
            $scope.submissions = response;

            for(let res in $scope.submissions){
                $scope.submissions[res].link = '#/courses/' + $routeParams.CRN + '/assignments/' + $routeParams.id + '/submissions/' + response[res]._id;
            }

            console.log($scope.submissions);
        })
        .error(function(response){
            alert(response);
            $location.path('/');
        });

    $scope.deleteSub = function(submitid){
        var response = confirm("Are you sure you want to delete your submission? This cannot be undone!");
        if(response === true){
            $http({
                method: 'POST',
                url: '/courses/' + $routeParams.CRN + '/assignments/' + $routeParams.id + '/submissions/' + submitid + '/delete',
                data: {
                    'username': $localStorage.user.username
                }
            })
                .success(function(response){
                    alert(response);
                    $location.reload();
                })
                .error(function(response){
                        alert(response);
                    }
                );
        }
    };
});

app.controller('CreateSubmissionController', function($scope, $localStorage, $sessionStorage, $http, $location,$routeParams){
    $scope.submitForm = function(){
        $http({
            method: 'POST',
            url: '/courses/' + $routeParams.CRN + '/assignments/' + $routeParams.id + '/submissions/create',
            data: {
                'filename' : $scope.newSubmission.filename,
                'username' : $localStorage.user.username
            },
        }).success(function(response){
            alert(response);
            $location.path('/courses/' + $routeParams.CRN + '/assignments/' + $routeParams.id );
        })
            .error(function(response){
                    // When a string is returned
                    if(typeof response === 'string'){
                        alert(response);
                    }
                    // When array is returned
                    else if (Array.isArray(response)){
                        // More than one message returned in the array
                        if(response.length > 1){
                            var messages = [],
                                allMessages;
                            for (var i = response.length - 1; i >= 0; i--) {
                                messages.push(response[i]['msg']);
                                if(response.length == 0){
                                    allMessages = messages.join(", ");
                                    alert(allMessages);
                                    console.error(response);
                                }
                            }
                        }
                        // Single message returned in the array
                        else{
                            alert(response[0]['msg']);
                            console.error(response);
                        }
                    }
                    // When something else is returned
                    else{
                        console.error(response);
                        alert("See console for error.");
                    }
                }
            );
    };

});

app.controller('SingleSubmissionsController', function($scope, $localStorage, $sessionStorage, $http, $location, $routeParams){
    $http({
        method: 'GET',
        url: '/courses/' + $routeParams.CRN + '/assignments/' + $routeParams.id + '/submissions/' + $routeParams.subid,
        params: {
            'username': $localStorage.user.username
        }
    })
        .success(function(response){
            $scope.submission = response;

            console.log($scope.submission);
        })
        .error(function(response){
            alert(response);
            $location.path('/');
        });

    $scope.deleteSubmission = function(){
        var response = confirm("Are you sure you want to delete your submission? This cannot be undone!");
        if(response === true){
            $http({
                method: 'POST',
                url: '/courses/' + $routeParams.CRN + '/assignments/' + $routeParams.id + '/submissions/' + $routeParams.subid + '/delete',
                data: {
                    'username': $localStorage.user.username
                }
            })
                .success(function(response){
                    alert(response);
                    $location.path('/courses/' + $routeParams.CRN + '/assignments/' + $routeParams.id);
                })
                .error(function(response){
                        alert(response);
                    }
                );
        }
    };

    $scope.newComment = function(){
        var comment = prompt("Enter your comment here", "");
        if(comment !== ""){
            $http({
                method: 'POST',
                url: '/courses/' + $routeParams.CRN + '/assignments/' + $routeParams.id + '/submissions/' + $routeParams.subid + '/newComment',
                data: {
                    'username': $localStorage.user.username,
                    'body' : comment
                }
            })
                .success(function(response){
                    alert(response);
                    $location.reload(true);

                })
                .error(function(response){
                        alert(response);
                    }
                );
        }
        else{
            alert("Please enter some text for your comment!")
        }
    };
});

app.controller('CreateSubmissionCommentController', function($scope, $localStorage, $sessionStorage, $http, $location,$routeParams){
    $scope.submitForm = function(){
        $http({
            method: 'POST',
            url: '/courses/' + $routeParams.CRN + '/assignments/' + $routeParams.id + '/submissions/create',
            data: {
                'body' : $scope.newSubmission.filename,
                'username' : $localStorage.user.username
            },
        }).success(function(response){
            alert(response);
            $location.path('/courses/' + $routeParams.CRN + '/assignments/' + $routeParams.id );
        })
            .error(function(response){
                    // When a string is returned
                    if(typeof response === 'string'){
                        alert(response);
                    }
                    // When array is returned
                    else if (Array.isArray(response)){
                        // More than one message returned in the array
                        if(response.length > 1){
                            var messages = [],
                                allMessages;
                            for (var i = response.length - 1; i >= 0; i--) {
                                messages.push(response[i]['msg']);
                                if(response.length == 0){
                                    allMessages = messages.join(", ");
                                    alert(allMessages);
                                    console.error(response);
                                }
                            }
                        }
                        // Single message returned in the array
                        else{
                            alert(response[0]['msg']);
                            console.error(response);
                        }
                    }
                    // When something else is returned
                    else{
                        console.error(response);
                        alert("See console for error.");
                    }
                }
            );
    };

});


/*********************************
 Routing
 *********************************/
app.config(function($routeProvider) {
    'use strict';

    $routeProvider.

    //Root
    when('/', {
        templateUrl: 'views/home.html',
        controller: 'HomeController'
    }).

    //Login page
    when('/account/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginController'
    }).

    //Account page
    when('/account', {
        templateUrl: 'views/account.html',
        controller: 'AccountController'
    }).

    //Create Account page
    when('/account/create', {
        templateUrl: 'views/create_account.html',
        controller: 'CreateAccountController'
    }).
    //Display Courses page
    when('/courses', {
        templateUrl: 'views/courses.html',
        controller: 'CoursesController'
    }).

    //Create Course Page
    when('/courses/create', {
        templateUrl: 'views/create_course.html',
        controller: 'CreateCourseController'
    }).
    when('/courses/:CRN', {
        templateUrl: 'views/single_course.html',
        controller: 'SingleCourseController'
    }).
    when('/courses/:CRN/assignments/create', {
        templateUrl: 'views/create_assignment.html',
        controller: 'CreateAssignmentController'
    }).
    when('/courses/:CRN/assignments', {
        templateUrl: 'views/assignments.html',
        controller: 'AssignmentsController'
    }).
    when('/courses/:CRN/assignments/:id', {
        templateUrl: 'views/single_assignment.html',
        controller: 'SingleAssignmentController'
    }).
    when('/courses/:CRN/assignments/:id/submissions',{
        templateUrl: 'views/submissions.html',
        controller: 'SubmissionsController'
    }).
    when('/courses/:CRN/assignments/:id/submissions/create', {
        templateUrl: 'views/create_submission.html',
        controller: 'CreateSubmissionController'
    }).
    when('/courses/:CRN/assignments/:id/submissions/:subid',{
        templateUrl: 'views/single_submission.html',
        controller: 'SingleSubmissionsController'
    });

});
